%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      Class ESIEA_report.cls                        %
%                                                                    %
%	Author : Armand Ito                                              %
%                                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Identification
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ESIEA_report}[2017/12/10 v1.0.0 Latex Format for ESIEA students report]

% Load the Base Class
\LoadClassWithOptions{report}


%%%%%%%%%%%%%%%%%%%%%%     Requirements     %%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{ifthen}						% Conditional commands

\RequirePackage[french]{babel}				% Langages

\RequirePackage[T1]{fontenc}					% To get french accents

\RequirePackage{graphicx,lipsum,mdframed}	% To get images

\RequirePackage{gensymb}						% To get °

\RequirePackage[utf8]{inputenc}				% To get table

\RequirePackage[table]{xcolor}				% To get color

\RequirePackage{fancyhdr}					% To get footer

\RequirePackage{palatino} 					% Old-style serif typeface

\RequirePackage{geometry}					% Set the Paper Size and margins

%%%%%%%%%%%%%%%%%%%%%%% End of Requirements %%%%%%%%%%%%%%%%%%%%%%

% Set the Tables Size and margins
\geometry{margin=1.0in}

%%%%%%%%%%%%%%%%%%%%%% Colors Configuration %%%%%%%%%%%%%%%%%%%%%%

\definecolor{blue_esiea}{HTML}{0086C7}
\definecolor{Mblack_esiea}{HTML}{1A171B}
\definecolor{Lblack_esiea}{HTML}{333333}
\definecolor{gray_esiea}{HTML}{e7e8e9}
\definecolor{orange_esiea}{HTML}{e8662c}
  
%%%%%%%%%%%%%%%%%% End of Colors Configuration %%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%% Specialized commands %%%%%%%%%%%%%%%%%%%%%%


\def\@heading{\relax}
\newcommand{\heading}[1]{\gdef\@heading{#1}}

\def\@organizer{\relax}
\newcommand{\organizer}[1]{\gdef\@organizer{#1}}

\def\@datemeeting{\relax}
\newcommand{\datemeeting}[1]{\gdef\@datemeeting{#1}}

\def\@hour{\relax}
\newcommand{\hour}[1]{\gdef\@hour{#1}}

\def\@address{\relax}
\newcommand{\address}[1]{\gdef\@address{#1}}

\def\@kind{\relax}
\newcommand{\kind}[1]{\gdef\@kind{#1}}

\def\@tutor{\relax}
\newcommand{\tutor}[1]{\gdef\@tutor{#1}}

\def\@members{\relax}
\newcommand{\members}[1]{\gdef\@members{#1}}

\def\@rank{\relax}
\newcommand{\rank}[1]{\gdef\@rank{#1}}

\def\@project{\relax}
\newcommand{\project}[1]{\gdef\@project{#1}}

\def\@redactor{\relax}
\newcommand{\redactor}[1]{\gdef\@redactor{#1}}

\def\@currentdate{\relax}
\newcommand{\currentdate}[1]{\gdef\@currentdate{#1}}

\def\@logo{\relax}
\newcommand{\logo}[1]{\gdef\@logo{\protect #1}}

\def\@TcolOne{\relax}
\newcommand{\TcolOne}[1]{\gdef\@TcolOne{\protect #1}}

\def\@TcolTwo{\relax}
\newcommand{\TcolTwo}[1]{\gdef\@TcolTwo{\protect #1}}

\def\@TcolThree{\relax}
\newcommand{\TcolThree}[1]{\gdef\@TcolThree{\protect #1}}

\def\@Content{\relax}
\newcommand{\Content}[1]{\gdef\@Content{\protect #1}}

%%%%%%%%%%%%%%%%%% End of Specialized commands %%%%%%%%%%%%%%%%%%%

% Create header and footer
\newcommand\makehf{
	\pagestyle{fancy}
	%\rhead{Axe : "Confiance Numérique et Sécurité" - Laboratoire (C+V)\degree}
	\lhead{\@project}
	\lfoot{\textbf{Date} : \today}
	\cfoot{\textbf{Rédacteur}: \@redactor}
	\rfoot{Page \thepage}
}

% Create the Reporthead 
\renewcommand{\maketitle}{\makecustomtitle}
\newcommand\makecustomtitle{
	\ifthenelse{\equal{\@logo}{\relax}}{}% conditional command
	{ 
		% Create With Logo
		\begin{center}
			 {\huge \textcolor{blue_esiea}{\textbf{Compte rendu n\degree \@rank }}}
		\end{center}
		\begin{center}
			\@logo
		\end{center}
		\begin{center}
			{\large \textbf{\@heading}}
		\end{center}
	}
	
	\vfill
}


% Create organisation table
\newcommand\makeorgatable{
	% Border color
	\arrayrulecolor[HTML]{333333}
	
	% Create table
	\begin{tabular}{ |s|p{7.4cm}|  }
		\hline
		\textcolor{white}{RÉUNION ORGANISÉE PAR} & \@organizer \\
		\hline
		\textcolor{white}{DATE DE LA RÉUNION}  & \@datemeeting \\
		\hline
		\textcolor{white}{HEURE} & \@hour \\
		\hline
		\textcolor{white}{LIEU} & \@address \\
		\hline
		\textcolor{white}{TYPE DE RÉUNION} & \@kind \\
		\hline
		\textcolor{white}{PARTICIPANTS} & \\
		\hfill {\small \textcolor{white}{Tuteur}} & \@tutor \\
		\hfill {\small \textcolor{white}{Tutorés}}& \@members \\
		\hline
	\end{tabular}
}

% Create first type table 
\newcommand\maketableOne{
\begin{table}[!ht]
	\begin{tabular}{|s|p{9.20cm}|}
	\hline
	%--Title-%   %-------------SubTitle----------------%	
	{\footnotesize \textbf{\textcolor{white}{\@TcolOne}}} & {\footnotesize \textbf{\@TcolTwo}} \\
	\hline
	\end{tabular}
	%--------Content--------%
	\begin{tabular}{| p{15.41cm} |}
		\hline
		%---------------------------------- Content ----------------------------------%
		\@Content \\
		\hline
	\end{tabular}
\end{table}
}

% Create second type table 
\newcommand\maketableTwo{
\begin{table}[!ht]
	\begin{tabular}{ |p{4.73cm}|p{4.23cm}|p{4.23cm}|}
		\hline
		%--Title-%   %-------------SubTitle----------------%	
		{\footnotesize \textbf{\cellcolor{blue_esiea}\textcolor{white}{\@TcolOne}}} & {\footnotesize \textbf{\@TcolTwo}} &  {\footnotesize \textbf{\@TcolThree}} \\
		\hline
	\end{tabular}
	\begin{tabular}{ |p{4.73cm}|p{4.23cm}|p{4.23cm}|}
		\hline
		%---------------------------------- Content ----------------------------------%
		\@Content
 		\hline
\end{tabular}
\end{table}
}